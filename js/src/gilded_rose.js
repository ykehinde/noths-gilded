function Item(name, sell_in, quality) {
  this.name = name;
  this.sell_in = sell_in;
  this.quality = quality;
}

var items = []

function update_quality() {
  let qualityCount = 1;
  const maxQuality = 50;
  const minQuality = 0;
  for (var i = 0; i < items.length; i++) {
    // Sulfuras, Hand of Ragnaros
    if (items[i].name === 'Sulfuras, Hand of Ragnaros') {
      items[i].quality = items[i].quality;
      items[i].sell_in = 0;
    }
    // Everything else
    if (items[i].name != 'Sulfuras, Hand of Ragnaros' ) {
      items[i].sell_in = items[i].sell_in - 1;

      if (items[i].name != 'Aged Brie' && items[i].name != 'Backstage passes to a TAFKAL80ETC concert') {
        if (items[i].name === 'Conjured Mana Cake' || items[i].sell_in < 0) {
          qualityCount = 2 ;
        }
        items[i].quality = items[i].quality != minQuality ? items[i].quality - qualityCount : minQuality;
      } else {
        if (items[i].name === 'Backstage passes to a TAFKAL80ETC concert') {
          if (items[i].sell_in < 0) {
            items[i].quality = minQuality;
          }
          if (items[i].sell_in <= 10 && items[i].sell_in > 5) {
            qualityCount = 2;
          } else if (items[i].sell_in <= 5 && items[i].sell_in >= 0) {
            qualityCount = 3;
          } else if (items[i].sell_in < 0) {
            items[i].quality = minQuality;
            qualityCount = minQuality;
          }
        }

        items[i].quality = (items[i].quality + qualityCount) >= 50 ? maxQuality : items[i].quality + qualityCount;
      }
    }
  }
}
