describe("Gilded Rose", function() {

  it("should foo", function() {
    items = [ new Item("foo", 0, 0) ];
    update_quality();
    expect(items[0].name).toEqual("foo");
  });

  it("should decrease by 1 to quality for generic items", function() {
    items = [ new Item("foo", 1, 30) ];
    update_quality();
    expect(items[0].quality).toEqual(29);
  });

  it("should decrease by 2 to quality for generic items if sell by date has passed", function() {
    items = [ new Item("foo", 0, 30) ];
    update_quality();
    expect(items[0].quality).toEqual(28);
  });

  it("should increase quality by 1 for concert tickets", function () {
    items = [new Item("Backstage passes to a TAFKAL80ETC concert", 30, 30)];
    update_quality();
    expect(items[0].quality).toEqual(31);
  });

  it("should increase quality by 2 for concert tickets under 10 days", function () {
    items = [new Item("Backstage passes to a TAFKAL80ETC concert", 9, 32)];
    update_quality();
    expect(items[0].quality).toEqual(34);
  });

  it("should increase quality by 3 for concert tickets under 5 days", function () {
    items = [new Item("Backstage passes to a TAFKAL80ETC concert", 4, 32)];
    update_quality();
    expect(items[0].quality).toEqual(35);
  });

  it("should increase quality by 3 for concert tickets under 5 days", function () {
    items = [new Item("Backstage passes to a TAFKAL80ETC concert", 4, 32)];
    update_quality();
    expect(items[0].quality).toEqual(35);
  });

  it("should increase quality past 50 for concert tickets", function () {
    items = [new Item("Backstage passes to a TAFKAL80ETC concert", 4, 49)];
    update_quality();
    expect(items[0].quality).toEqual(50);
  });

  it("should not increase or decrease quality for concert tickets past sell by date", function () {
    items = [new Item("Backstage passes to a TAFKAL80ETC concert", -1, 49)];
    update_quality();
    expect(items[0].quality).toEqual(0);
  });

  it("should not increase quality for Sulfuras and sell by to be 0", function () {
    items = [new Item("Sulfuras, Hand of Ragnaros", 9, 120)];
    update_quality();
    expect(items[0].quality).toEqual(120);
    expect(items[0].sell_in).toEqual(0);
  });

});
